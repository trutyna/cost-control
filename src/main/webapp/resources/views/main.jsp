<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Cost Control App</title>
    <link href="${pageContext.servletContext.contextPath}/resources/css/app.css" rel="stylesheet"></head>
<body>
<jsp:include page="menu.jsp" />
Aplikacja do kontrolowania wydatków

<sec:authorize access="hasRole('USER')">
    <h1>Jestes zalogowany</h1>
</sec:authorize>
</body>
</html>
