package pl.sda.costcontrol.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.sda.costcontrol.bo.CostService;
import pl.sda.costcontrol.dto.CostDto;
import pl.sda.costcontrol.dto.NewCostDto;

import java.util.List;

/**
 * @author trutyna
 */
@RestController
@RequestMapping("/api/cost/")
public class RestApiController {

    private final CostService service;

    @Autowired
    public RestApiController(CostService service) {
        this.service = service;
    }

    @GetMapping(value = "all")
    public List<CostDto> findAll() {
        return service.findCosts();
    }

    @DeleteMapping
    public void delete(@RequestParam("id") Long id) {
        service.deleteCost(id);
    }

    @GetMapping("/{id}")
    public CostDto findById(@PathVariable("id") Long id) {
        return service.findCostDetails(id);
    }

    @PostMapping
    public void createCost(@RequestBody NewCostDto dto) {
        //create new cost
    }
}
