package pl.sda.costcontrol.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author trutyna
 */
@Entity
@Table(name = "costs")
public class Cost {

    @Id
    @GeneratedValue
    private Long id;
}
