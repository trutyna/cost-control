package pl.sda.costcontrol.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.costcontrol.entity.Cost;

/**
 * @author trutyna
 */
@Repository
public interface CostRepository extends JpaRepository<Cost, Long> {
}
