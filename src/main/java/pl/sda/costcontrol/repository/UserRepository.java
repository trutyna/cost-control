package pl.sda.costcontrol.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.costcontrol.entity.User;
/**
 * @author Tomasz Rutyna (SG0301553)
 * @since Mar 08, 2018
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>
{
    User findByName(String name);
}
