package pl.sda.spring.cost.control;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author trutyna
 */
public class PasswordEncoder {

    @Test
    public void encodePassword() {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        System.out.println(encoder.encode("1qaz2wsx"));

    }
}
